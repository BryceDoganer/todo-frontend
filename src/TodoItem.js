import React, {Component} from 'react';

const TodoItem = ({name, completed, onDelete, onToggle}) => (
  <li>
    <span 
      className="todo-item"
      style={{
        textDecoration: completed ? 'line-through' : 'none'
      }}
      onClick={onToggle}
    >
      {name}
    </span>
    <span onClick={onDelete}> X </span>
  </li>
);

export default TodoItem;